package main

import "fmt"

func main() {
	var age int32 = 37
	const isCool = true
	var size float32 = 2.3

	// name := "Brad"
	// size := 1.3

	name, age := "Brad", 32

	fmt.Println(name, age, isCool)
	fmt.Printf("%T\n", size)
}
