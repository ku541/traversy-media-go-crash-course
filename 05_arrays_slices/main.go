package main

import "fmt"

func main() {
	// var fruits [2]string

	// fruits[0] = "Apple"
	// fruits[1] = "Orange"

	fruits := [2]string{"Apple", "Orange"}

	colors := []string{"Red", "Green", "Blue", "Yellow", "Black", "White"}

	fmt.Println(fruits)

	fmt.Println(colors)
	fmt.Println(len(colors))
	fmt.Println(colors[2:4])
}
