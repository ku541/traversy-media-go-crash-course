package main

import (
	"fmt"
	"strconv"
)

// Person definition
type Person struct {
	// firstName string
	// lastName  string
	// city      string
	// gender    string
	// age       int
	firstName, lastName, city, gender string
	age                               int
}

func (p Person) greet() string {
	return "Hello, my name is " + p.firstName + " " + p.lastName + " and I am " + strconv.Itoa(p.age)
}

func (p *Person) hasBirthday() {
	p.age++
}

func (p *Person) getMarried(spouseLastName string) {
	if p.gender == "m" {
		return
	}

	p.lastName = spouseLastName
}

func main() {
	person1 := Person{firstName: "Samantha", lastName: "Smith", city: "Boston", gender: "f", age: 25}
	person2 := Person{"Samantha", "Smith", "Boston", "f", 25}

	fmt.Println(person1.age, person2.age)

	person1.age++

	fmt.Println(person1)

	person1.hasBirthday()
	person1.getMarried("Williams")

	fmt.Println(person1.greet())
}
