package main

import "fmt"

func main() {
	fmt.Println(getSum(1, 2))
}

func greet(name string) string {
	return "Hello " + name
}

func getSum(num1, num2 int) int {
	return num1 + num2
}
